package com.hcmm;

import java.util.ArrayList;
import java.util.List;

public class MemberOperation implements OperationInterface {

    private List<Member> members = new ArrayList<Member>();
    private FileStorage fileStorage = new FileStorage();

    public void removeMember(String name,String mobile) {
        List<Member> members = fileStorage.read();
        for (int i = 0; i < members.size(); i++) {
            if (members.get(i).getName().equals(name)) {
                if (members.get(i).getMobile().equals(mobile) ) {
                    members.remove(i);
                }
            }
        }
        fileStorage.write(members);
    }

    public List passQuery(String arg1) {
        List<Member> members = fileStorage.read();
        int i=0;
        
        // find query
        while (i<members.size()) {
            if (!members.get(i).getPass().equals(arg1)) {
                members.remove(i);
                i = 0;
            }else{
                i++;
            }
        }

        //sort data
        for (int ii = 0; ii < members.size(); ii++) {
            for (int jj = ii+1; jj < members.size(); jj++) {
                if(members.get(ii).getName().compareTo(members.get(jj).getName())>0){
                    Member tmp=members.get(ii);
                    members.set(ii, members.get(jj));
                    members.set(jj, tmp);
                }
                if(members.get(ii).getName().compareTo(members.get(jj).getName())==0){
                    if(members.get(ii).getMobile().compareTo(members.get(jj).getMobile())>0){
                        Member tmp=members.get(ii);
                        members.set(ii, members.get(jj));
                        members.set(jj, tmp);
                    }
                }
            }
        }
        return members;
    }

    public void addMember(Member member) {
        if (fileStorage.read().size() != 0) {
            members = fileStorage.read();
        }
        members.add(member);
        fileStorage.write(members);
    }

    public void updateMember(Member member) {
        List<Member> members = fileStorage.read();
        for (int i = 0; i < members.size(); i++) {
            if (members.get(i).getName().equals(member.getName())) {
                if (members.get(i).getMobile().equals(member.getMobile())) {
                    members.set(i, member);
                }
            }
        }
        fileStorage.write(members);
    }

    public Member getMember(String name,String mobile) {
        List<Member> members = fileStorage.read();
        for (Member member : members) {
            if (member.getName().equals(name) ) {
                if (member.getMobile().equals(mobile) ) {
                    return member;
                }
            }
        }
        return null;
    }


    public List<Member> getAllMembers() {
        return fileStorage.read();
    }
}
