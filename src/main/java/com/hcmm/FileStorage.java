package com.hcmm;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class FileStorage {
    public String dataFile="MemberData.txt";
    public String instructionFile="Instruction.txt";
    public String resultFile="resultFile.txt";
    public String reportFile="reportFile.txt";
    public void fileCheck(){
        File f = new File(dataFile);
        if(!(f.exists() && !f.isDirectory())) { 
            try {
                String str = "[]";
                BufferedWriter writer = new BufferedWriter(new FileWriter(dataFile));
                writer.write(str);
                
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        f = new File(instructionFile);
        if(!(f.exists() && !f.isDirectory())) { 
            try {
                String isrt = "- add name;birthday;pass;mobile;fee\n- delete name;mobile\n- query pass type\n- query age fee\n- query all";
                BufferedWriter iwr = new BufferedWriter(new FileWriter(instructionFile));
                iwr.write(isrt);                
                iwr.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        f = new File(resultFile);
        if(!(f.exists() && !f.isDirectory())) { 
            try {
                f.createNewFile();
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
        f = new File(reportFile);
        if(!(f.exists() && !f.isDirectory())) { 
            try {
                f.createNewFile();
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
    }

    public void write(List<Member> members) {

        ObjectMapper mapperWrite = new ObjectMapper();
        try {
            String json = mapperWrite.writerWithDefaultPrettyPrinter().writeValueAsString(members);
            FileWriter file = new FileWriter(dataFile);
            file.write(json);
            file.flush();
            file.close();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Member> read() {
        StringBuilder contentBuilder = new StringBuilder();
        ObjectMapper mapperRead = new ObjectMapper();
        try {
            BufferedReader br = new BufferedReader(new FileReader(dataFile));
            String currentLine;
            while ((currentLine = br.readLine()) != null) {
                contentBuilder.append(currentLine).append("\n");
            }
            br.close();
            return mapperRead.readValue(contentBuilder.toString(), new TypeReference<List<Member>>(){});
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public void readFile(String filename) {
        try {
            FileReader freader = new FileReader(filename);
            BufferedReader br = new BufferedReader(freader);
            String s;
            while((s = br.readLine()) != null) {
            System.out.println(s);
            }
            freader.close();            
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
    public void saveQuery(String filename, String  result){
        try {
            File file = new File(filename);
            FileWriter fr = new FileWriter(file, true);
            BufferedWriter br = new BufferedWriter(fr);
            br.write(result);
            
            br.close();
            fr.close();   
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
}