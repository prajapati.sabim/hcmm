package com.hcmm;

public class Member {
	//Field Variables
	private String name;
	private String birthDate;
	private String pass;
	private String mobile;
	private String fee;
	
	//Methods 
	//Constructors
	public Member() {
		this.name = "";
		this.birthDate = ""; 
		this.pass = "";
		this.mobile = "";
		this.fee = "";
	}

	public boolean validate(MemberOperation member) {
		Member check=member.getMember(this.name, this.mobile);
		if(check!=null){
			return false;
		}
		return true;
	}
	
	public Member(String name, String birthDate, String pass,String mobile, String fee) {
		this.name = name; 
		this.birthDate = birthDate;
		this.pass = pass;
		this.mobile = mobile;
		this.fee = fee;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getBirthDate() {
		return this.birthDate;
	}
	
	public String getPass() {
		return this.pass;
	}
	public String getMobile() {
		return this.mobile;
	}
	public String getFee() {
		return this.fee;
	}
	
	public String toString() {
		return "Name: " +getName() +", BirthDate: " +getBirthDate() +", Pass: " +getPass() +", Mobile: " +getMobile() +", Fee: " +getFee();
	}
	
	//End of Member class
}
