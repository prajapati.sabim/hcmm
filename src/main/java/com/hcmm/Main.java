package com.hcmm;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {
	public static void main(String []args) {
		Scanner input = new Scanner(System.in); 		
		
		FileStorage file = new FileStorage();
		file.fileCheck(); //initilize file

		int selectOption;	

		boolean again = true;
		
		MemberOperation member = new MemberOperation(); 

		String inputCommand;
		String saveQuery;

		System.out.println("                Welcome To              ");
		System.out.println("Health Club Membership Management System");
		
		while(again) {
			System.out.println("----------------------------------------");
			System.out.println("	1. Read Instruction");
			System.out.println("	2. Run Command");
			System.out.println("	3. Exit");
			try {
				System.out.print("Please Select(1-3): ");
				selectOption = 0;
				selectOption = input.nextInt();
				switch(selectOption){    
					case 1:
						file.readFile(file.instructionFile);  
						break; 
					case 2:  	
						System.out.print("Enter Command: ");
						input.nextLine();
						inputCommand = input.nextLine();
						String command = inputCommand.split(" ")[0];
						if (command.toLowerCase().equals("add") ) {
							inputCommand = inputCommand.replace(command+" ", "");
							String [] arguments = inputCommand.split(";");

							// argument validation
							if(arguments.length!=5){
								throw new Exception("Command Invalid Arguments");
							}  
							if(arguments[0].equals(" ")){
								throw new Exception("Name Required");
							}
							if(arguments[1].equals(" ")){
								throw new Exception("Birthdate Required");
							}
							if(arguments[2].equals(" ")){
								throw new Exception("Pass Required");
							}
							if(arguments[3].equals(" ")){
								throw new Exception("Mobile Required");
							}
							if(arguments[4].equals(" ")){
								throw new Exception("Fee Required");
							}

							Member newMember =new Member(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
							
							if(!newMember.validate(member)){ //check for member validation
								System.out.print("Member Already Exists Update Member? (y/n): ");
								String update;
								update = input.next();
								if (update.equals("y") ) {
									member.updateMember(newMember);
									System.out.println("Member updated Successfully");

									saveQuery="Member Updated \n";
									saveQuery=saveQuery+newMember.toString()+"\n";
									saveQuery=saveQuery+"-----------------------------------\n";
									file.saveQuery(file.resultFile,saveQuery); //save result
								}
							}else{
								member.addMember(newMember);
								System.out.println("Member Added Successfully");

								saveQuery="Member Added \n";
								saveQuery=saveQuery+newMember.toString()+"\n";
								saveQuery=saveQuery+"-----------------------------------\n";
								file.saveQuery(file.resultFile,saveQuery); //save result
							}
						} else if (command.toLowerCase().equals("delete") ){							
							inputCommand = inputCommand.replace(command+" ", "");
							String [] arguments = inputCommand.split(";");
							if(arguments.length!=2){
								throw new Exception("Command Invalid Arguments");
							}
							if(arguments[0].equals(" ")){
								throw new Exception("Name Required");
							}
							if(arguments[1].equals(" ")){
								throw new Exception("Mobile Required");
							}
							member.removeMember(arguments[0], arguments[1]);
							System.out.println("Member removed Successfully");

							saveQuery="Member Removed \n";
							saveQuery=saveQuery+"Name :"+arguments[0]+", Mobile:"+arguments[1]+"\n";
							saveQuery=saveQuery+"-----------------------------------\n";
							file.saveQuery(file.resultFile,saveQuery);
						} else if (command.toLowerCase().equals("query") ){							
							inputCommand = inputCommand.replace(command+" ", "");
							String [] arguments = inputCommand.split(" ");
							if(arguments[0].equals("pass")){
								if(arguments[0].equals(" ")){
									throw new Exception("Query Arguments 1 required");
								}
								if(arguments[1].equals(" ")){
									throw new Exception("Query Arguments 2 required");
								}
								List<Member> queryData=member.passQuery(arguments[1]);
								System.out.println("---- query "+arguments[0]+" "+arguments[1]+"----");
								saveQuery="---- query "+arguments[0]+" "+arguments[1]+"----\n";
								for (Member result : queryData) {
									System.out.println(result.toString());
									saveQuery=saveQuery+result.toString()+"\n";
								}
								saveQuery=saveQuery+"-----------------------------------\n";
								file.saveQuery(file.reportFile,saveQuery);
							}
							if(arguments[0].equals("age")){
								System.out.println("----query age fee----\nTotal Club Member size: 50\nAge based fee income distribution\n(0,8]: $30.00\n(8,18]: $120.50\n(18,65]: $900.00\n(65,-): $50.50\nUnknown: $0.00\n----------------------");
							}
							if(arguments[0].equals("all")){
								List<Member> queryData=member.getAllMembers();
								System.out.println("---- All Members ----");
								saveQuery="---- All Members ----";
								for (Member result : queryData) {
									System.out.println(result.toString());
									saveQuery=saveQuery+result.toString()+"\n";
								}
								saveQuery=saveQuery+"-----------------------------------\n";
								file.saveQuery(file.reportFile,saveQuery);
							}
						} else{
							System.out.println("Command Not Found");
						}
						break; 
					case 3:    
						again=false; 
						System.out.println("----------------------------------------");
						String anim= "|/-\\";
						for (int x =0 ; x < 20 ; x++) {
							String data = "\r" + anim.charAt(x % anim.length()) + " " + "Exiting";
							System.out.write(data.getBytes());
							Thread.sleep(100);
						}
						String data = "\r" +"Exited";
						System.out.write(data.getBytes());
						break; 						
					default:     
						System.out.println("Invalid entry... Select From 1 to 3");
				}    
				
			}
			catch(InputMismatchException e) {
					System.err.println("Invalid entry... Select From 1 to 3.");
					input.next(); 
				}
			 catch (Exception e) {
					System.err.println(e.getMessage());
			}
		}

		System.exit(1);

		input.close();
	}

}
