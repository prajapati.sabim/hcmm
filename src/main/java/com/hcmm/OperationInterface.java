package com.hcmm;

import java.util.List;

public interface OperationInterface {

    void removeMember(String name,String mobile);

    void addMember(Member member);

    void updateMember(Member member);


    Member getMember(String name,String mobile);

    List<Member> getAllMembers();
}
